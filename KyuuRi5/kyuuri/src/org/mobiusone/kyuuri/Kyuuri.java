/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import twitter4j.AsyncTwitter;
import twitter4j.AsyncTwitterFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 *
 * @author mobius notificationの表示タイミングを吟味のこと。
 */
public class Kyuuri extends Application
{

    static ApplicationConfig applicationConfig = new ApplicationConfig();
    static List<User> userList = new ArrayList<User>();
    static PreparingTwitter preparingTwitter = new PreparingTwitter();
    static Notification notification;

    @Override
    public void start(Stage stage) throws Exception
    {
        stage.setTitle("KyuuRi 5");
        Parent root = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        notification = new Notification();
        //notification.openNotificationFrame();
        openAuthorizationWizzard();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

    /**
     * GUIによるログインウィザードです。 commandLineAuthorizationに代わってこちらを利用してください。
     */
    void openAuthorizationWizzard() throws IOException
    {
        Stage stage = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("AuthenticationWizzard.fxml")));
        stage.setTitle("アカウントの認証");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @Deprecated GUIによるログインが実装されるまでのつなぎです。 特別な理由がない限りGUI実装を利用してください。
     */
    static void authenticateByCommandLine()
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true)
        {
            System.out.println("このURLにて認証を行ってください");
            try
            {
                preparingTwitter.openAuthorizationPage();
            } catch (URISyntaxException ex)
            {
                Logger.getLogger(Kyuuri.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex)
            {
                Logger.getLogger(Kyuuri.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(preparingTwitter.getAuthorizationURL());
            System.out.println("PINコードを入力してください。");
            String pin = null;
            try
            {
                pin = br.readLine();
            } catch (IOException ex)
            {
                Logger.getLogger(Kyuuri.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (authenticateByPin(pin))
            {
                break;
            }
        }
        userList.add(new User(preparingTwitter.twitter));
    }

    /**
     * 認証後に自動的にオブジェクトを再生成するために移動しました。
     * @param pin 認証に使うPINコードを渡します。
     * @return 
     */
    static boolean authenticateByPin(String pin)
    {
        //PINコードから認証を行います。
        //認証成功でtrueを返します。
        try
        {
            if (pin.length() > 0)
            {
                preparingTwitter.accessToken = preparingTwitter.twitter.getOAuthAccessToken(preparingTwitter.requestToken, pin);
                System.out.println("認証に成功しました。");
                userList.add(new User(preparingTwitter.twitter));
                preparingTwitter = preparingTwitter.regeneratePreparingTwitter();
                return true;
            } else
            {
                preparingTwitter.accessToken = preparingTwitter.twitter.getOAuthAccessToken();
            }
        } catch (TwitterException te)
        {
            if (401 == te.getStatusCode())
            {
                System.out.println("認証に失敗しました。");
            } else
            {
                te.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 新たなアカウントの認証に使う一時的なTwitterオブジェクトです。
     * 認証が完了したら速やかにListにaddし、オブジェクトを再生成してください。
     */
    static class PreparingTwitter
    {

        AsyncTwitter twitter = AsyncTwitterFactory.getSingleton();
        RequestToken requestToken = null;
        AccessToken accessToken = null;

        public PreparingTwitter()
        {
            twitter.setOAuthConsumer(applicationConfig.CK, applicationConfig.CS);    
            try
            {
                requestToken = twitter.getOAuthRequestToken();
            } catch (TwitterException ex)
            {
                Logger.getLogger(Kyuuri.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        PreparingTwitter regeneratePreparingTwitter()
        {
            twitter = TwitterFactory.getSingleton();
            accessToken=null;
            try
            {
                requestToken = twitter.getOAuthRequestToken();
            } catch (TwitterException ex)
            {
                Logger.getLogger(Kyuuri.class.getName()).log(Level.SEVERE, null, ex);
            }
            return this;
        }

        /**
         * 認証に使うURLを返します。
         *
         * @return 認証に使うURLです。
         */
        String getAuthorizationURL()
        {
            return requestToken.getAuthorizationURL();
        }

        /**
         *
         * @Deprecated 認証ページをデフォルトのブラウザで起動します。
         * 推奨されません。可能ならばJavaFXのWebViewを使って内部ブラウザから認証してください。
         *
         * @throws URISyntaxException ブラウザ起動中の例外をスローします。
         * @throws IOException ブラウザ起動中の例外をスローします。
         */
        void openAuthorizationPage() throws URISyntaxException, IOException
        {
            Desktop desktop = Desktop.getDesktop();
            desktop.browse(new URI(getAuthorizationURL()));
        }
    }
}
