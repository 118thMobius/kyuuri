/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri6.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mobius
 */
public abstract class Config
{
    String fileName;
    Properties properties;
    public Config(String name) throws IOException
    {
        fileName = name;
    }
    
    public void generate()
    {
        FileChannel srcChannel = null;
        FileChannel toChannel = null;
        try
        {
            srcChannel = new FileInputStream("config/defaultconfig/default.config").getChannel();
            toChannel = new FileOutputStream("config/application.config").getChannel();
            srcChannel.transferTo(0, srcChannel.size(), toChannel);
        } catch (IOException ex)
        {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }finally
        {
            try
            {
                srcChannel.close();
                toChannel.close();
            } catch (IOException ex)
            {
                Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public boolean exists(){
        return false;
    }
    
    public void load() throws FileNotFoundException, IOException
    {
        properties.load(new FileInputStream(fileName));
    }
    
    public void save() throws FileNotFoundException, IOException
    {
        properties.store(new FileOutputStream(fileName), null);
    }
    
    public String getProperty(String key)
    {
        return properties.getProperty(key);
    }
    
    public void setProperty(String key,String data)
    {
        properties.setProperty(key, data);
    }
}
