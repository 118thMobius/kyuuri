/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mobius
 */
public class ApplicationConfig
{
    
    static Properties properties = null;
    static final String OS_NAME = System.getProperty("os.name").toLowerCase();
    static boolean customCkCs = false;
    public static String CK = "pvQxYYRo7aqYrGfmIfRKIksEs";
    public static String CS = "G2H00ala3kl3cKRf1nig80LynrRVlT5NkMVgODmlMv56X8Lf50";
    
    Properties applicationConfigProperties = new Properties();
    String applicationConfigPropertiesFilePass = "config" + File.separator+"application.config";
    
    ApplicationConfig()
    {
        System.out.println(System.getProperty("os.name") + "環境で起動します。");
        if (new File(applicationConfigPropertiesFilePass).exists())
        {
            System.out.println(applicationConfigPropertiesFilePass + "を読み込みます");
        } else
        {
            System.out.println("アプリケーション設定が存在していません。デフォルト設定を読み込みます。");
            FileChannel srcChannel = null;
            FileChannel toChannel = null;
            
            try
            {
                srcChannel = new FileInputStream("config/defaultconfig/default.config").getChannel();
                toChannel = new FileOutputStream("config/application.config").getChannel();
                srcChannel.transferTo(0, srcChannel.size(), toChannel);
            } catch (IOException ex)
            {
                Logger.getLogger(ApplicationConfig.class.getName()).log(Level.SEVERE, null, ex);
            } finally
            {
                try
                {
                    srcChannel.close();
                    toChannel.close();
                } catch (IOException ex)
                {
                    Logger.getLogger(ApplicationConfig.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    static void loadConfig()
    {
        try
        {
            properties.load(new FileInputStream("config/application.config"));
        } catch (IOException ex)
        {
            Logger.getLogger(ApplicationConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void SaveConfig()
    {
        
    }
    
}
