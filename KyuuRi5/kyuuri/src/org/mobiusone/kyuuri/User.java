/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 *
 * @author mobius
 */
public class User {
    Twitter twitter = TwitterFactory.getSingleton();
    UserConfig userConfig = new UserConfig();

    public User(Twitter t)
    {
        //新しいアカウントを生成した場合に使います。
        //twitterを引数として与えることで、アカウント設定ファイルの作成などを行います。
        twitter = t;
        
    }
    public User(String userID)
    {
        //すでに存在するアカウントを呼び出すのに使います。
        //IDを与えることで、ユーザーのコンフィグなどをロードして、twitterを適切にインスタンス化します。
    }
    
    void Tweet(String message) throws TwitterException
    {
        //このアカウントを利用してツイートします。
        //messageに与えられた文字列のみをつぶやきます。
        //ツイート中に発生した例外をスローします。
        //一時的に起きうる例外の処理を行っています。要確認
        try {

            twitter.updateStatus(message);
        } catch (TwitterException ex) {
            switch(ex.getStatusCode())
            {
                case 403:
                    System.out.println("重複したツイートです。");
                    break;
                default:
                    throw ex;
            }
        }
    }
    void getTimeline()
    {
        //タイムラインの配列を返します。
    }
}