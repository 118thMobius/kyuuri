/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author mobius
 */
public class MainScreenController implements Initializable
{

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        initializeAnimation();
        // TODO
    }    
    
    private void initializeAnimation()
    {
        showQuickMenuTransition = new FadeTransition(Duration.millis(200),quickMenuAnchorPane);
        showQuickMenuTransition.setFromValue(0);
        showQuickMenuTransition.setToValue(1);
    }
    
    private FadeTransition showQuickMenuTransition;
    
    @FXML
    private StackPane mainStackPane;
    @FXML
    private AnchorPane quickMenuAnchorPane;
    @FXML
    private TextArea tweetArea;
    @FXML
    private Label numberLabel;
    
    @FXML
    private void openQuickSettingMenu(MouseEvent event)
    {
        mainStackPane.getChildren().remove(quickMenuAnchorPane);
        mainStackPane.getChildren().add(quickMenuAnchorPane);
        quickMenuAnchorPane.setOpacity(0);
        showQuickMenuTransition.play();;
    }
    
    @FXML
    private void closeQuickSettingMenu(MouseEvent event)
    {
        mainStackPane.getChildren().remove(quickMenuAnchorPane);
    }
    
    @FXML
    private void countLengthOfTweet(KeyEvent event)
    {
        
        if(tweetArea.getText().length()>100)
        {
            numberLabel.setStyle("-fx-text-fill:red");
        }
        else
        {
            numberLabel.setStyle("-fx-text-fill:black");
        }
        numberLabel.setText(String.valueOf(140-tweetArea.getText().length()));
    }
    
    
}
