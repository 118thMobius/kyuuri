/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author mobius
 */
public class Notification
{

    Stage stage;
    /**
     * 実装を考えたほうがいい。
     */
    public Notification() throws IOException
    {
        stage = new Stage(StageStyle.TRANSPARENT);
        stage.setHeight(Screen.getPrimary().getVisualBounds().getMaxY());
        stage.setWidth(Screen.getPrimary().getVisualBounds().getMaxX());
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("NotificationFrame.fxml")));
        scene.setFill(null);
        stage.setAlwaysOnTop(true);
        stage.setTitle("KyuuRi通知センター");
        stage.setScene(scene);
    }
    
    /**
     * 通知を表示するためのステージを表示します。
     * このステージは透明で、アプリケーション一覧にも表示されません。
     * 
     * @throws IOException 
     */
    public void openNotificationFrame()
    {
        stage.show();
    }
}
