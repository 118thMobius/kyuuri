/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobiusone.kyuuri;

import java.awt.Color;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import org.omg.CORBA.BAD_CONTEXT;

/**
 * FXML Controller class
 *
 * @author mobius
 */
public class AuthenticationWizzardController implements Initializable
{
    
    int pageIndex = 0;
    int pageCount;
    List<Node> pageList = new ArrayList<Node>();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        pageList.add(page0);
        pageList.add(page1);
        pageList.add(page2);
        pageCount=pageList.size();
        pageIndicatorLabel.setText(pageIndex+1+"/"+pageCount);
        // TODO
        authenticationWebView.getEngine().load(Kyuuri.preparingTwitter.getAuthorizationURL());
    }    
    
    @FXML
    private StackPane mainStackPane;
    
    @FXML
    private HBox page0;
    
    @FXML
    private VBox page1;
    
    @FXML
    private HBox page2;
    
    @FXML
    private TextField pinForm;
    
    @FXML
    private Label descriptionLabel;
    
    @FXML
    private Button rightButton;
    
    @FXML
    private WebView authenticationWebView;
    
    @FXML
    private Label pageIndicatorLabel;
    
    @FXML
    private void openPreviousPage(MouseEvent event)
    {
        if(0 < pageIndex)
        {
            pageIndex--;
            mainStackPane.getChildren().remove(pageList.get(pageIndex));
            mainStackPane.getChildren().add(pageList.get(pageIndex));
            pageIndicatorLabel.setText(pageIndex+1+"/"+pageCount);
            
            if(pageIndex==1)
            {
                rightButton.setDisable(true);
            }
            else
            {
                rightButton.setDisable(false);
            }
        }
    }
    
    @FXML
    private void openNextPage(MouseEvent event)
    {
        if(pageIndex < pageCount-1)
        {
            pageIndex++;
            mainStackPane.getChildren().remove(pageList.get(pageIndex));
            mainStackPane.getChildren().add(pageList.get(pageIndex));
            pageIndicatorLabel.setText(pageIndex+1+"/"+pageCount);  
            
            if(pageIndex==1)
            {
                rightButton.setDisable(true);
            }
            else
            {
                rightButton.setDisable(false);
            }
        }
        
    }
    
    @FXML
    private void authenticatePin(MouseEvent event)
    {
        if (Kyuuri.authenticateByPin(pinForm.getText().trim()))
        {
            openNextPage(new MouseEvent(null, 0, 0, 0, 0, MouseButton.NONE, 0, true, true, true, true, true, true, true, true, true, true, null));
        }
        else
        {
            descriptionLabel.setText("認証に失敗しました。正しくPINコードが入力されているか確認してください。");
            descriptionLabel.setStyle("-fx-text-fill:red");
        }
    }
}
